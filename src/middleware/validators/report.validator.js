const { body } = require('express-validator');

exports.validateReport=[
    body('id').isNumeric(),
    body('fecha').isDate(),
    body('factura').isNumeric(),
    body('categoria').isAlphanumeric(),
    body('descripcion').isAlphanumeric(),
    body('total').isNumeric(),
];