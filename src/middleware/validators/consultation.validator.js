
const { body } = require('express-validator');

exports.validateConsultation = [
  body('carnet').isAlphanumeric(),
  body('temperature').isNumeric(),
  body('reason').isAlphanumeric(),
];