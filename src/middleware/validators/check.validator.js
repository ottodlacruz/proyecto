
const { body } = require('express-validator');

exports.validateCheck = [
  body('quantity').isCurrency(),
  body('check_no').isNumeric(),
  body('account_no').isNumeric(),
]