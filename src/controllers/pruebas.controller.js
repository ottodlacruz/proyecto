const PruebaModel = require('../models/pruebas.model')

const { validationResult } = require('express-validator')

exports.guardarEvaluacion = (req, res) => {
    console.log('dentro de registroEvaludacion controller')
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    const pruebaData = new PruebaModel(req.body)
    //console.log(pruebaData)

    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res
            .status(400)
            .send({ success: false, message: 'Por Favor ingrese todos los datos ' })
    } else {
        PruebaModel.registroEvaludacion(pruebaData, (error, result) => {
            if (error) {
                res.send(error)
            } else {
                //.....logica_consolidado_resultados
                if (pruebaData.nivel_prueba === 'Nivel_1') {
                    console.log('Nivel_1')
                    const jsonResultado = {
                        pregunta_1: pruebaData.pregunta_1 === 'A' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_2: pruebaData.pregunta_2 === 'Z' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_3: pruebaData.pregunta_3 === 'G' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_4: pruebaData.pregunta_4 === 'Y' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_5: pruebaData.pregunta_5 === 'C' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_6: pruebaData.pregunta_6 === 'E' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_7: pruebaData.pregunta_7 === 'N' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_8: pruebaData.pregunta_8 === 'W' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_9: pruebaData.pregunta_9 === 'V' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_10: pruebaData.pregunta_10 === 'M' ? 'CORRECTO' : 'INCORRECTO',
                        cod_resultado: result.insertId,
                        fecha_creo: pruebaData.fecha_creo
                    }
                    console.log('JSONresult -> ' + JSON.stringify(jsonResultado))
                    //---saveResult
                    PruebaModel.registroResultados(jsonResultado, (error_1, result_p1) => {
                        if (error_1) {
                            res.send(error_1)
                        } else {
                            res.json({
                                success: true,
                                message: 'Datos Guardos Exitosamente - Q1',
                                data: result_p1
                            })
                        }
                    })
                    //-----------
                } else {
                    console.log('Nivel_2')
                    const jsonResultado2 = {
                        pregunta_1: pruebaData.pregunta_1 === 'A' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_2:
                            pruebaData.pregunta_2 === 'AMARILLO' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_3: pruebaData.pregunta_3 === 'AZUL' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_4: pruebaData.pregunta_4 === 'C' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_5: pruebaData.pregunta_5 === 'EL' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_6: pruebaData.pregunta_6 === 'ELLA' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_7: pruebaData.pregunta_7 === 'F' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_8: pruebaData.pregunta_8 === 'G' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_9: pruebaData.pregunta_9 === 'ROJO' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_10: pruebaData.pregunta_10 === 'V' ? 'CORRECTO' : 'INCORRECTO',
                        cod_resultado: result.insertId,
                        fecha_creo: pruebaData.fecha_creo
                    }
                    console.log('JSONresult -> ' + JSON.stringify(jsonResultado2))
                    //---saveResult
                    PruebaModel.registroResultados(jsonResultado2, (error_2, result_p2) => {
                        if (error_2) {
                            res.send(error_2)
                        } else {
                            res.json({
                                success: true,
                                message: 'Datos Guardos Exitosamente - Q2',
                                data: result_p2
                            })
                        }
                    })
                    //-----------
                }
            }
        })
    }
}

exports.tablaResultadosQ1 = (req, res) => {
    PruebaModel.tablaResultadosQ1((error, resultados) => {
        if (error) {
            res.send(error)
        } else {
            console.log(resultados)
            res.send(resultados)
        }
    })
}

exports.tablaResultadosQ2 = (req, res) => {
    PruebaModel.tablaResultadosQ2((error, resultados) => {
        if (error) {
            res.send(error)
        } else {
            console.log(resultados)
            res.send(resultados)
        }
    })
}

//External Users

exports.guardarEvaluacionExternalUsers = (req, res) => {
    console.log('dentro de registroEvaludacion controller')
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    const pruebaData = new PruebaModel(req.body)
    //console.log(pruebaData)

    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        return res
            .status(400)
            .send({ success: false, message: 'Por Favor ingrese todos los datos ' })
    } else {
        PruebaModel.registroEvaludacionExternal(pruebaData, (error, result) => {
            if (error) {
                res.send(error)
            } else {
                //.....logica_consolidado_resultados
                if (pruebaData.nivel_prueba === 'Nivel_1') {
                    console.log('Nivel_1')
                    const jsonResultado = {
                        pregunta_1: pruebaData.pregunta_1 === 'A' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_2: pruebaData.pregunta_2 === 'Z' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_3: pruebaData.pregunta_3 === 'G' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_4: pruebaData.pregunta_4 === 'Y' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_5: pruebaData.pregunta_5 === 'C' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_6: pruebaData.pregunta_6 === 'E' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_7: pruebaData.pregunta_7 === 'N' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_8: pruebaData.pregunta_8 === 'W' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_9: pruebaData.pregunta_9 === 'V' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_10: pruebaData.pregunta_10 === 'M' ? 'CORRECTO' : 'INCORRECTO',
                        cod_resultado: result.insertId,
                        fecha_creo: pruebaData.fecha_creo
                    }
                    console.log('JSONresult -> ' + JSON.stringify(jsonResultado))
                    //---saveResult
                    PruebaModel.registroResultadosExternal(jsonResultado, (error_1, result_p1) => {
                        if (error_1) {
                            res.send(error_1)
                        } else {
                            res.json({
                                success: true,
                                message: 'Datos Guardos Exitosamente - Q1',
                                data: result_p1
                            })
                        }
                    })
                    //-----------
                } else {
                    console.log('Nivel_2')
                    const jsonResultado2 = {
                        pregunta_1: pruebaData.pregunta_1 === 'A' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_2:
                            pruebaData.pregunta_2 === 'AMARILLO' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_3: pruebaData.pregunta_3 === 'AZUL' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_4: pruebaData.pregunta_4 === 'C' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_5: pruebaData.pregunta_5 === 'EL' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_6: pruebaData.pregunta_6 === 'ELLA' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_7: pruebaData.pregunta_7 === 'F' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_8: pruebaData.pregunta_8 === 'G' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_9: pruebaData.pregunta_9 === 'ROJO' ? 'CORRECTO' : 'INCORRECTO',
                        pregunta_10: pruebaData.pregunta_10 === 'V' ? 'CORRECTO' : 'INCORRECTO',
                        cod_resultado: result.insertId,
                        fecha_creo: pruebaData.fecha_creo
                    }
                    console.log('JSONresult -> ' + JSON.stringify(jsonResultado2))
                    //---saveResult
                    PruebaModel.registroResultadosExternal(jsonResultado2, (error_2, result_p2) => {
                        if (error_2) {
                            res.send(error_2)
                        } else {
                            res.json({
                                success: true,
                                message: 'Datos Guardos Exitosamente - Q2',
                                data: result_p2
                            })
                        }
                    })
                    //-----------
                }
            }
        })
    }
}

exports.tablaResultadosExtQ1 = (req, res) => {
    PruebaModel.tablaResultadosExtQ1((error, resultados) => {
        if (error) {
            res.send(error)
        } else {
            console.log(resultados)
            res.send(resultados)
        }
    })
}

exports.tablaResultadosExtQ2 = (req, res) => {
    PruebaModel.tablaResultadosExtQ2((error, resultados) => {
        if (error) {
            res.send(error)
        } else {
            console.log(resultados)
            res.send(resultados)
        }
    })
}
