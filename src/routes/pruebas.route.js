const express = require('express')
const router = express.Router()

const pruebasController = require('../controllers/pruebas.controller')

// Evaluacion Internal Users
router.post('/respuestas', pruebasController.guardarEvaluacion)
router.get('/tablaresultadosQ1', pruebasController.tablaResultadosQ1)
router.get('/tablaresultadosQ2', pruebasController.tablaResultadosQ2)
// Evaluacion External Users
router.post('/respuestasExternalUsers', pruebasController.guardarEvaluacionExternalUsers)
router.get('/tablaresultados_ext_Q1', pruebasController.tablaResultadosExtQ1)
router.get('/tablaresultados_ext_Q2', pruebasController.tablaResultadosExtQ2)

module.exports = router
