var dbConn = require('../db/db-connection')

var dataPrueba = function (data) {
    this.usuario = data.usuario
    this.pregunta_1 = data.pregunta_1
    this.pregunta_2 = data.pregunta_2
    this.pregunta_3 = data.pregunta_3
    this.pregunta_4 = data.pregunta_4
    this.pregunta_5 = data.pregunta_5
    this.pregunta_6 = data.pregunta_6
    this.pregunta_7 = data.pregunta_7
    this.pregunta_8 = data.pregunta_8
    this.pregunta_9 = data.pregunta_9
    this.pregunta_10 = data.pregunta_10
    this.nivel_prueba = data.nivel_prueba
    this.fecha_creo = dateFormat()
}

function dateFormat() {
    const date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
    return date
}

dataPrueba.registroEvaludacion = (jsonResultados, result) => {
    console.log('dentro de registroEvaludacion model')
    //console.log(jsonResultados.respuestas[0].respuesta_1)
    dbConn.query('INSERT INTO tb_resultados SET ?', jsonResultados, (err, res) => {
        if (err) {
            console.log('Error while creating consultation', err)
            result(null, err)
        } else {
            console.log('Prueba created succesfully')
            result(null, res)
        }
    })
}

dataPrueba.registroResultados = (jsonResultados, result) => {
    console.log('dentro de registroResultados model')
    //console.log(jsonResultados.respuestas[0].respuesta_1)
    dbConn.query('INSERT INTO tb_consolidado_resultados SET ?', jsonResultados, (err, res) => {
        if (err) {
            console.log('Error while creating consultation', err)
            result(null, err)
        } else {
            console.log('Prueba created succesfully')
            result(null, res)
        }
    })
}

dataPrueba.tablaResultadosQ1 = (result) => {
    dbConn.query(
        `SELECT COSRES.*, RESUL.usuario ,RESUL.nivel_prueba 
         FROM tb_consolidado_resultados COSRES
         LEFT OUTER JOIN tb_resultados RESUL 
         ON COSRES.cod_resultado = RESUL.cod_resultado
         WHERE RESUL.nivel_prueba = 'Nivel_1';`,
        (err, res) => {
            if (err) {
                console.log('Error while fetching ResultadosQ1', err)
                result(null, err)
            } else {
                console.log('ResultadosQ1 fetched succesfully')
                result(null, res)
            }
        }
    )
}

dataPrueba.tablaResultadosQ2 = (result) => {
    dbConn.query(
        `SELECT COSRES.*, RESUL.usuario ,RESUL.nivel_prueba 
         FROM tb_consolidado_resultados COSRES
         LEFT OUTER JOIN tb_resultados RESUL 
         ON COSRES.cod_resultado = RESUL.cod_resultado
         WHERE RESUL.nivel_prueba = 'Nivel_2';`,
        (err, res) => {
            if (err) {
                console.log('Error while fetching ResultadosQ2', err)
                result(null, err)
            } else {
                console.log('ResultadosQ2 fetched succesfully')
                result(null, res)
            }
        }
    )
}

//External Users

dataPrueba.registroEvaludacionExternal = (jsonResultados, result) => {
    console.log('dentro de registroEvaludacion model')
    //console.log(jsonResultados.respuestas[0].respuesta_1)
    dbConn.query(
        'INSERT INTO tb_resultados_usuarios_externos SET ?',
        jsonResultados,
        (err, res) => {
            if (err) {
                console.log('Error while creating consultation', err)
                result(null, err)
            } else {
                console.log('Prueba created succesfully')
                result(null, res)
            }
        }
    )
}

dataPrueba.registroResultadosExternal = (jsonResultados, result) => {
    console.log('dentro de registroResultados model')
    //console.log(jsonResultados.respuestas[0].respuesta_1)
    dbConn.query(
        'INSERT INTO tb_consolidado_resultados_externos SET ?',
        jsonResultados,
        (err, res) => {
            if (err) {
                console.log('Error while creating consultation', err)
                result(null, err)
            } else {
                console.log('Prueba created succesfully')
                result(null, res)
            }
        }
    )
}

dataPrueba.tablaResultadosExtQ1 = (result) => {
    dbConn.query(
        `SELECT COSRES.*, RESUL.usuario ,RESUL.nivel_prueba 
         FROM tb_consolidado_resultados_externos COSRES
         LEFT OUTER JOIN tb_resultados_usuarios_externos RESUL 
         ON COSRES.cod_resultado = RESUL.cod_resultado
         WHERE RESUL.nivel_prueba = 'Nivel_1';`,
        (err, res) => {
            if (err) {
                console.log('Error while fetching ResultadosQ1', err)
                result(null, err)
            } else {
                console.log('ResultadosQ1 fetched succesfully')
                result(null, res)
            }
        }
    )
}

dataPrueba.tablaResultadosExtQ2 = (result) => {
    dbConn.query(
        `SELECT COSRES.*, RESUL.usuario ,RESUL.nivel_prueba 
         FROM tb_consolidado_resultados_externos COSRES
         LEFT OUTER JOIN tb_resultados_usuarios_externos RESUL 
         ON COSRES.cod_resultado = RESUL.cod_resultado
         WHERE RESUL.nivel_prueba = 'Nivel_2';`,
        (err, res) => {
            if (err) {
                console.log('Error while fetching ResultadosQ2', err)
                result(null, err)
            } else {
                console.log('ResultadosQ2 fetched succesfully')
                result(null, res)
            }
        }
    )
}

module.exports = dataPrueba
