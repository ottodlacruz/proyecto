const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const HttpException = require('./src/utils/HttpException.utils');
const errorMiddleware = require('./src/middleware/error.middleware');
const userRouter = require('./src/routes/user.route');
const evaluacionRouter = require('./src/routes/pruebas.route');

const bcrypt = require('bcrypt');

// Init express
const app = express();
// Init environment
dotenv.config();

app.use(express.json({ limit: '50mb' }));
// enabling cors for all requests by using cors middleware
app.use(cors());
// Enable pre-flight
app.options('*', cors());

const port = Number(process.env.PORT || 3331);
app.get('/', (req, res) => res.send('Hello World!'));

app.use('/api/v1/user', userRouter);
app.use('/api/v1/evaluacion', evaluacionRouter);

// 404 error
app.all('*', (req, res, next) => {
    const err = new HttpException(404, 'Endpoint Not Found');
    next(err);
});

// Error middleware
app.use(errorMiddleware);
app.set('json replacer', function (key, value) {
    if (this[key] instanceof Date) {
        // Your own custom date serialization
        value = this[key].toLocaleDateString('en-GB');
    }

    return value;
});

// starting the server
app.listen(port, () => console.log(`🚀 Server running on port ${port}!`));

module.exports = app;
